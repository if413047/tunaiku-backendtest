<?php

require_once 'include/DB_Function.php';
$db = new DB_Functions();

// json response array
$response = array("error" => FALSE);

if (isset($_POST['date'])) {

    // menerima parameter POST
    $date = $_POST['date'];
    //data past 7 days
    $data = $db->loanTracked($date);
    $response["error"] = FALSE;
    if (count($data) > 0) {
        //rata-rata dalam 7 hari
        $_avg = $data["summary"] / 7;
        $response["data"]["jumlah"] = $data["jumlah"];
        $response["data"]["summary"] = $data["summary"];
        $response["data"]["average"] = round($_avg);
    } else {
        $response["_msg"] = "Data tidak ada";
    }
    echo json_encode($response);
} else {
    $response["error"] = TRUE;
    $response["_msg"] = "Mohon untuk mengisi data yang diperlukan";
    echo json_encode($response);
}
?>