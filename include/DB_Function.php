<?php

class DB_Functions {

    private $conn;
    private static $Male = 'L';
    private static $Female = 'P';

    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // koneksi ke database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }

    // destructor
    function __destruct() {

    }

    public function isValidKtp($noKtp, $birthDate, $sex) {
//        $noKtp = '3522582509010002';
        $array = $this->pecahString($noKtp);

        //tahun - bulan - hari $birthdate
        $yy = substr($birthDate, 2, 2);
        $mm = substr($birthDate, 5, 2);
        $dd = substr($birthDate, 8, 2);
        $female = $dd + 40;
        if ($sex == self::$Female) {
            $birthDate = $female . '' . $mm . '' . $yy;
        } else if ($sex == self::$Male) {
            $birthDate = $dd . '' . $mm . '' . $yy;
        }
        $KTPbirthDate = $array[1];
//        if (strcmp($birthDate, $KTPbirthDate) == 0) {
        if ($birthDate == $KTPbirthDate) {
            return true;
        } else {
            return false;
        }
    }

    private function pecahString($string) {
        // mendapatkan jumlah substring yang terbentuk jika dibagi dengan 2
        $m = ceil(strlen($string) / 6);

        // inisialisasi hasil
        $hasil = array();

        // proses pemecahan subtring
        for ($i = 0; $i <= $m - 1; $i++) {
            $hasil[$i] = substr($string, $i * 6, 6);
        }

        return $hasil;
    }

    public function loanTracked($date) {
        //ambil data 7 hari termasuk date yang dipilih
        $stmt = $this->conn->prepare("SELECT COUNT(id) AS jumlah, SUM(amount) AS summary FROM loan_tbl WHERE active=10 AND (loan_date BETWEEN ADDDATE(?,-7) AND ? )");

        $stmt->bind_param("ss", $date, $date);

        if ($stmt->execute()) {
            $data = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $data;
        } else {
            return NULL;
        }
    }

    public function getRate($periode) {
        //ambil rate dari periode
        $stmt = $this->conn->prepare("SELECT flat FROM mst_interest WHERE active=10 AND periode = ?");

        $stmt->bind_param("s", $periode);

        if ($stmt->execute()) {
            $data = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $data;
        } else {
            return NULL;
        }
    }

}
