/*
SQLyog Ultimate v10.42 
MySQL - 5.5.5-10.1.34-MariaDB : Database - db_backend_test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_backend_test` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_backend_test`;

/*Table structure for table `loan_tbl` */

DROP TABLE IF EXISTS `loan_tbl`;

CREATE TABLE `loan_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_mst` int(11) NOT NULL,
  `loan_type` int(11) NOT NULL,
  `loan_date` date NOT NULL,
  `amount` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`),
  KEY `fk-loan_tbl-userID` (`user_mst`),
  KEY `fk-loan_tbl-loanType` (`loan_type`),
  CONSTRAINT `fk-loan_tbl-loanType` FOREIGN KEY (`loan_type`) REFERENCES `mst_interest` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk-loan_tbl-userID` FOREIGN KEY (`user_mst`) REFERENCES `mst_user_tbl` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `loan_tbl` */

insert  into `loan_tbl`(`id`,`user_mst`,`loan_type`,`loan_date`,`amount`,`created_at`,`updated_at`,`active`) values (1,1,1,'2019-08-23',100000000,'2019-08-23 17:10:49','2019-08-23 17:10:50',10),(2,2,1,'2019-08-24',50000000,'2019-08-24 17:12:05','2019-08-24 17:12:14',10),(3,1,1,'2019-08-26',30000000,'2019-08-26 17:12:43','2019-08-26 17:12:50',10),(4,3,1,'2019-08-30',10000000,'2019-08-30 17:24:15','2019-08-30 17:24:24',10),(5,4,2,'2019-08-31',20000000,'2019-08-31 17:28:18','2019-08-31 17:28:20',10);

/*Table structure for table `mst_interest` */

DROP TABLE IF EXISTS `mst_interest`;

CREATE TABLE `mst_interest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `periode` int(11) NOT NULL,
  `flat` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `mst_interest` */

insert  into `mst_interest`(`id`,`periode`,`flat`,`created_at`,`updated_at`,`active`) values (1,12,1.68,'2019-08-26 17:09:08','2019-08-26 17:09:08',10),(2,18,1.68,'2019-08-26 17:09:08','2019-08-26 17:09:08',10),(3,24,1.59,'2019-08-26 17:09:08','2019-08-26 17:09:08',10),(4,30,1.59,'2019-08-26 17:09:08','2019-08-26 17:09:08',10),(5,36,1.59,'2019-08-26 17:09:08','2019-08-26 17:09:08',10);

/*Table structure for table `mst_user_tbl` */

DROP TABLE IF EXISTS `mst_user_tbl`;

CREATE TABLE `mst_user_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ktp_no` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `sex` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `active` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `mst_user_tbl` */

insert  into `mst_user_tbl`(`id`,`ktp_no`,`birthdate`,`sex`,`name`,`created_at`,`updated_at`,`active`) values (1,'3522582509010002','2001-09-25','L','Dony','0000-00-00 00:00:00','0000-00-00 00:00:00',10),(2,'3522586609010002','2001-09-26','P','Tania','0000-00-00 00:00:00','0000-00-00 00:00:00',10),(3,'3522586709010002','2001-09-27','P','Sulastri','0000-00-00 00:00:00','0000-00-00 00:00:00',10),(4,'3522582809010002','2001-09-28','L','Alfonso','0000-00-00 00:00:00','0000-00-00 00:00:00',10);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
