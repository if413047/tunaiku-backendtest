<?php

require_once 'include/DB_Function.php';
$db = new DB_Functions();

// json response array
$response = array("error" => FALSE);

//
//$ktp_no = '3522585608190002';
//$birthdate = '2019-08-16';
//$sex = 'Female';
//
//$a = $db->isValidKtp($ktp_no, $birthdate, $sex);
//if ($a) {

if (isset($_POST['ktp_no']) && isset($_POST['birthdate']) && isset($_POST['sex'])) {
//    menerima parameter POST
    $ktp_no = $_POST['ktp_no'];
    $birthdate = $_POST['birthdate'];
    $sex = $_POST['sex'];
    // Cek jika no ktp valid atau tidak
    if ($db->isValidKtp($ktp_no, $birthdate, $sex)) {
        // ktp valid
        $response["error"] = FALSE;
        $response["data"]["ktp"] = $ktp_no;
        $response["data"]["_msg"] = "Valid";
        echo json_encode($response);
    } else {
        // ktp invalid
        $response["error"] = TRUE;
        $response["data"]["ktp"] = $ktp_no;
        $response["_msg"] = "Invalid";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Mohon untuk mengisi data yang diperlukan";
    echo json_encode($response);
}
?>