<?php

require_once 'include/DB_Function.php';
$db = new DB_Functions();

// json response array
$response = array("error" => FALSE);

if (isset($_POST['date']) && isset($_POST['amount']) && isset($_POST['periode'])) {

// menerima parameter POST ( date, amount, periode )
    $date = $_POST['date'];
    $amount = $_POST['amount'];
    $periode = $_POST['periode'];
    $getRate = $db->getRate($periode);
    $rate = $getRate['flat'];
    if ($rate) {
        $i = 0;
        $month = 1;
        while ($i < $periode) {
            $capital = round($amount / $periode);
            $interest = round($amount * $rate / 100);
            $total = $capital + $interest;
            $response["data"][$i]["months_installment"] = $month;
            $response["data"][$i]["due_date"] = date('Y-m-d', strtotime('+' . $month . ' months', strtotime($date)));
            $response["data"][$i]["capital"] = $capital;
            $response["data"][$i]["interest"] = $interest;
            $response["data"][$i]["total"] = $total;
            $i++;
            $month++;
        }
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Periode Pinjaman tidak didukung";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Mohon untuk mengisi data yang diperlukan";
    echo json_encode($response);
}
?>